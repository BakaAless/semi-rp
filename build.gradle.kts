plugins {
    kotlin("jvm") version "1.8.21"
    id("io.papermc.paperweight.userdev") version "1.5.5"
}

group = "fr.alessevan"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.papermc.io/repository/maven-public/")

}

dependencies {
    paperweight.paperDevBundle("1.20-R0.1-SNAPSHOT")
    compileOnly("com.gitlab.avatar-tales:avatar-api:64f01c9212")
    compileOnly("org.jetbrains.kotlin:kotlin-stdlib:1.8.0")
}


tasks {
    assemble {
        dependsOn(reobfJar)
    }

    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }

    javadoc {
        options.encoding = Charsets.UTF_8.name()
    }
    processResources {
        filteringCharset = Charsets.UTF_8.name()
        expand("version" to project.version)
    }
}

