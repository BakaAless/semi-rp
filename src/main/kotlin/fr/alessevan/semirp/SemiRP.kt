package fr.alessevan.semirp

import fr.alessevan.api.AvatarAPI
import fr.alessevan.api.module.AvatarModule
import fr.alessevan.semirp.launcher.SemiRPLauncher
import java.nio.file.Path

class SemiRP(plugin: SemiRPLauncher?, api: AvatarAPI?, dataFolder: Path?) :
    AvatarModule<SemiRPLauncher>(plugin, api, dataFolder) {
    override fun onEnable() {

    }

    override fun onDisable() {

    }
}