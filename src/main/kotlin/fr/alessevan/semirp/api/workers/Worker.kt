package fr.alessevan.semirp.api.workers

import fr.alessevan.semirp.api.jobs.Job

interface Worker {

    fun getJobID() : Int

    fun getJob() : Job

    fun getJobLevel() : Int

    fun getJobXP() : Int

}