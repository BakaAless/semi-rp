package fr.alessevan.semirp.launcher

import fr.alessevan.api.AvatarAPI
import fr.alessevan.semirp.SemiRP
import org.bukkit.plugin.java.JavaPlugin

class SemiRPLauncher : JavaPlugin() {

    override fun onEnable() {
        AvatarAPI.getAPI().registerModule(this, SemiRP::class.java)
    }

    override fun onDisable() {
        AvatarAPI.getAPI().unregisterModule(SemiRP::class.java)
    }
}